package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public static long factorial(int n) {
        if(n==1){
            return 1 ;
        }
        if(n==0){
            return 1 ;
        }
        return  n * factorial(n-1);
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public static long fibonacci(int n) {
        long a =1 , b = 1 ;
        for(int i = 1 ; i <= n-2 ; i ++){
            long temp = 0 ;
            temp = b ;
            b = a + b ;
            a = temp ;
        }
        return b;
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public static String reverse(String word) {
        StringBuilder reverse = new StringBuilder(word);
        reverse = reverse.reverse();
        return reverse.toString();
    }


    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public static boolean isPalindrome(String line) {
        String[] part1 = line.split(" ");
        int n = 0 ;
        String word = "" ;
        for(int i = 0 ; i < part1.length ; i++ ) {
            word = word + part1[i] ;
        }
        StringBuilder word2 = new StringBuilder(word) ;
        boolean equals1 = word.equalsIgnoreCase(word2.reverse().toString());
        return equals1 ;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public static char[][] dotPlot(String str1, String str2) {
        char[][] churt = new char[str2.length()+1][str1.length()*2+3];
        char[][] churt2 = new char[str2.length()+1][str1.length()*2+3];
        for(int i = 0 ; i < str1.length()*2+3 ; i++){
            for(int z = 0 ; z <= str2.length() ; z++){
                churt[z][i] = ' ';
            }
        }
        int j = 0 ;
        for(int i = 4  ; i < str1.length()*2+3 ; i=i+2){
            churt[0][i] = str1.charAt(j);
            j++ ;
        }
        j = 0 ;
        for(int i = 1  ; i <= str2.length() ; i++){
            churt[i][0] = str2.charAt(j);
            j++ ;
        }
        for(int i = 4  ; i < str1.length()*2+3 ; i=i+2){
            for(int z = 1  ; z <= str2.length() ; z++){
                if( churt[z][0] == churt[0][i] ){
                    churt[z][i] = '*';
                }
            }
        }
        j=0 ;
        int h=0 ;
        for(int i = 4  ; i < str1.length()*2+3 ; i=i+2){
            for(int z = 1  ; z <= str2.length() ; z++){
                churt2[j][h] = churt[z][i] ;
               j++;
            }
            h++;
            j=0;
        }
        return churt2 ;
    }
}
