package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
private int Conter;
    public void count()
    {
        if(Conter < 9999)
            Conter += 1;
    }
    public int getValue()
    {
        return Conter;
    }

    public void setValue(int value) throws IllegalValueException
    {
        if(value > 9999 || value < 0)
        {
            throw new
                    IllegalValueException();
        }
        Conter = value;
    }
}